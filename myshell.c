// WORK DONE BY 'TEAM RS' SMART O IDIMA AND RAJESH RAVULA

#include <sys/types.h>  
#include <errno.h>     
#include <stdio.h>      
#include <sys/wait.h>
#include <stdlib.h>     
#include <string.h>
#include <unistd.h>
#define MAX_LINE_SIZE 20

int main() {
    while(1) {
        printf("myshell> ");
        
	//using malloc to allocate memory space
        char* temp = (char*)malloc(MAX_LINE_SIZE);
        char* line = (char*)malloc(MAX_LINE_SIZE);
        fgets(temp, MAX_LINE_SIZE, stdin); //getting input from user
        strcpy(line, temp);
        char* operation;
        char* parameters[MAX_LINE_SIZE];

        line = strtok(line, "\n");
        operation = strtok(temp, " \n"); // get operation
        
        int c = 0;
        
        parameters[c] = strtok(NULL, ";\n");
        while (parameters[c] != NULL) { 		// walk through other line
            parameters[++c] = strtok(NULL, ";\n");
        }
        if(operation!=NULL)
        
	    {

	        //Terminate process and exit when the operation call exit
	        if(strcmp(operation,"exit")==0)
	        {
	            break;
	            
	        }

	        //fork a new process and have the parent wait for the child to complete if operation is exec
	        else if(strcmp(operation,"exec")==0)
	        {
	            int i;
	            char *args[MAX_LINE_SIZE];
	            for (i = 0; i < c; i++) {
	                int a = 0;
	                args[a] = strtok(parameters[i], " \n");
	                while (args[a] != NULL) {
	                    args[++a] = strtok(NULL, " \n");
	                    
	                }
	                int status;
	                pid_t result = fork();
	                if(result>0){
	                    wait(&status);
	                    if (status != 0) {
	                        printf("myshell: No such file or directory ");
	                        int i = 0;
	                        for (i = 0; i < a - 1; i++) {
	                            printf("%s ", args[i]);
	                            
	                        }
                            printf("\n");
                        }
	                }
	                else if (result == 0) {
	                     printf("myshell: starting child pid %d\n", getpid());
                         execvp(args[0], args); // Calling the execvp() system call
                         return getpid();
	                }
                }
	        
	        }
	        else
	        {
	            printf("myshell: %s is not a valid operation\n",operation);
	        
	        }
	    }
	//free memory allocated
	free(temp);
	free(line);

    }
	return 0;
}
